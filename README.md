# VDI Voking

Provide api to navigate and control chrome browser.

## Services

- Python
- Selenium
- Docker
- Chrome
- noVnc
- FastAPI

## Develop

Make sure docker engine is installed and running on your machine

## Linux

```bash
bash ./develop
```

OR

```bash
docker compose -f docker-compose.yml -f docker-compose.local.yml up --build -d
```

## Windows

```cmd
develop
```
