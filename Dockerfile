FROM python:3.9

LABEL maintainer="mayank.g@smarter.codes"

WORKDIR /app

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get install -y \
    python3-xlib python3-tk python3-dev \
    gconf-service libasound2 libatk1.0-0 libcairo2 \
    libcups2 libfontconfig1 libgdk-pixbuf2.0-0 libgtk-3-0 \
    libnspr4 libpango-1.0-0 libxss1 fonts-liberation \
    libnss3 lsb-release xdg-utils \
    portaudio19-dev ffmpeg libsm6 libxext6 dbus-x11 scrot

RUN DEBIAN_FRONTEND=noninteractive apt update && \
    apt install -y \
    x11vnc xvfb xserver-xephyr novnc python3-websockify \
    pulseaudio

COPY ./google-chrome.deb /app
RUN dpkg -i google-chrome.deb; apt-get -fy install

COPY ./chromedriver_linux.zip /app
RUN unzip chromedriver_linux.zip
RUN mv chromedriver /usr/bin/chromedriver
RUN chown root:root /usr/bin/chromedriver
RUN chmod +x /usr/bin/chromedriver

ENV DISPLAY :0

ENV ROOTPATH /
ENV VNCPASSWD keepcalm
ENV VNCPATH websockify
ENV VNCHOST http://localhost:6080/vnc.html
ENV APIHOST http://localhost:3000

COPY ./requirements.txt /app/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

COPY ./app /app/app
COPY ./run.sh /app/run.sh
COPY ./run_vnc.sh /app/run_vnc.sh
COPY ./run_sound.sh /app/run_sound.sh
COPY ./run_display.sh /app/run_display.sh

CMD ["/bin/bash", "/app/run.sh"]
