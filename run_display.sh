until Xvfb $DISPLAY -screen 0 1280x720x24
do
    echo "################################################"
    echo "#                                              #"
    echo "# Virtual display failed to start. Retrying... #"
    echo "#                                              #"
    echo "################################################"

    sleep 3
done