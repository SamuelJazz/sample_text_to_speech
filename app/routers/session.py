from os import environ

from app.controllers.automate_jitsi import jitsi_sales_ai
from app.controllers.browser import get_driver
from app.helpers.decorators import handleAPIError
from fastapi import APIRouter, Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

router = APIRouter()

ROOTPATH = environ.get("ROOTPATH")

USERAGENT = environ.get("USERAGENT")
HTTP_PROXY = environ.get("HTTP_PROXY", "")
HTTPS_PROXY = environ.get("HTTPS_PROXY", "")

if ROOTPATH == "/":
    ROOTPATH = ""


@router.get("/", response_class=HTMLResponse)
@handleAPIError
async def landing(request: Request):
    VNCPATH = environ.get('VNCPATH')
    VNCHOST = environ.get('VNCHOST')
    VNCADDR = f'{VNCHOST}?path={VNCPATH}'

    context = {
        "request": request,
        "VNCHOST": VNCADDR,
    }

    templates = Jinja2Templates(directory="/app/app/templates")
    return templates.TemplateResponse("index.html", context)


@router.get('/visit')
@handleAPIError
async def visit(url: str, restart: bool = False):
    driver = get_driver(restart)
    driver.delete_all_cookies()
    jitsi_sales_ai(url)
    return {"success": True}


@router.post('/join-meeting')
async def join_meeting():
    # Perform actions to join the meeting using the provided meeting_info
    # Example code:
    # join_meeting_function(meeting_info)
    return {"message": "Successfully joined the meeting"}

@router.post('/leave-meeting')
async def leave_meeting():
    # Perform actions to leave the meeting using the provided meeting_info
    # Example code:
    # leave_meeting_function(meeting_info)
    return {"message": "Successfully left the meeting"}