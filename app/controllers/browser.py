from app.helpers.logger import logger, show_error
from selenium.webdriver.chrome import webdriver
from selenium.webdriver.chrome.options import Options

driver = webdriver.WebDriver
driver = None


@show_error
def get_driver(restart=False):
    global driver

    if not driver or restart:
        try:
            if driver:
                driver.quit()
        except Exception as e:
            logger.exception(e)

        chrome_options = Options()
        chrome_options.add_argument('--test-type')
        chrome_options.add_argument('--kiosk')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-setuid-sandbox')
        chrome_options.add_argument('--use-fake-ui-for-media-stream')
        chrome_options.add_argument('--window-size=1280,720')
        chrome_options.add_argument('--window-position=0,0')
        chrome_options.add_argument('--check-for-update-interval=31536000')
        chrome_options.add_argument('--noerrdialogs')
        chrome_options.add_argument('--disable-infobars')
        chrome_options.add_argument('--start-maximized')
        chrome_options.add_argument(
            '--disable-password-manager-reauthentication')
        chrome_options.add_argument('--disable-features=UserAgentClientHint')
        chrome_options.add_experimental_option(
            "excludeSwitches", ["enable-automation"])
        
        driver = webdriver.WebDriver(options=chrome_options)
        driver.get("about://blank")

    return driver
