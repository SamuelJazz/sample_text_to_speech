import string
from datetime import date, timedelta
from time import sleep

from app.controllers.automate import Runner
from app.helpers.logger import logger, show_error
from selenium.common.exceptions import ElementNotInteractableException
from selenium.webdriver.common.by import By


@show_error
def jitsi_sales_ai(url: str):

    r = Runner()
    driver = r.driver
    driver.get(url)

    if "join.jit.smarter.codes" not in url:
        return

    sleep(1)

    CONFIG = {
        "actions": [
            ['i', "Sales AI Bot",
                '/html/body/div/div/div/div[4]/div[1]/div/div/div[1]/div[1]/div/input'],
            ['c', '/html/body/div/div/div/div[4]/div[1]/div/div/div[2]/div/div/div/div[1]/div/div/div[1]'],
            ['c', '/html/body/div/div/div/div[4]/div[1]/div/div/div[1]/div[2]/div/div']
        ]
    }

    sleep(1)
    r.run(config=CONFIG)
