from time import sleep

from app.controllers.browser import get_driver
from app.helpers.logger import logger, show_error
from selenium.common.exceptions import (ElementClickInterceptedException,
                                        ElementNotInteractableException)
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class Runner:
    @show_error
    def __init__(self, config=None):
        self.driver = get_driver()
        self.config = config
        self.delay = 5

    @show_error
    def BY(self, by, match, multiple=False):
        return WebDriverWait(self.driver, self.delay).until(EC.presence_of_element_located((by, match)) if not multiple else EC.presence_of_all_elements_located((by, match)))

    @show_error
    def __xpath(self, xpath):
        return self.BY(By.XPATH, xpath)

    @show_error
    def XPATH(self, xpath):
        return self.BY(By.XPATH, xpath)

    @show_error
    def close(self):
        self.driver.close()

    @show_error
    def click(self, xpath):
        elem = self.__xpath(xpath)
        if not elem:
            logger.warning(f"Element not found: {xpath}")
            return
        try:
            elem.click()
        except ElementClickInterceptedException:
            self.driver.execute_script("arguments[0].click();", elem)
        except ElementNotInteractableException:
            logger.warning(f"Element not interactable: {xpath}")

    @show_error
    def send(self, xpath, data):
        elem = self.__xpath(xpath)
        if not elem:
            logger.warning(f"Element not found: {xpath}")
            return
        try:
            elem.clear()
            elem.send_keys(data)
        except ElementNotInteractableException:
            logger.warning(f"Element not interactable: {xpath}")

    @show_error
    def back(self):
        self.driver.back()

    @show_error
    def forward(self):
        self.driver.forward()

    @show_error
    def refresh(self):
        self.driver.refresh()

    @show_error
    def run(self, config=None):
        if config:
            self.config = config
        for data in self.config.get("actions"):
            sleep(1)
            if isinstance(data, list):
                if data[0] == "c":
                    self.click(data[1])
                elif data[0] == "i":
                    self.send(data[2], data[1])
            if isinstance(data, str):
                if data == 'b':
                    self.back()
                elif data == 'f':
                    self.forward()
                elif data == 'r':
                    self.refresh()

    @show_error
    def __enter__(self):
        self.run()

    @show_error
    def __exit__(self, exc_type, exc_val, exc_tb):
        # self.close()
        pass

    @show_error
    def run_url(self, url):
        self.driver.get(url)
