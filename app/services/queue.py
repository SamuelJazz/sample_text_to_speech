import sounddevice as sd
import soundfile as sf
import speech_recognition as sr
import io
import threading
import queue
from app.helpers.logger import logger

sample_rate = 44100
duration = 5
audio_queue = queue.Queue()
text_queue = queue.Queue()

def record_audio():
    while True:
        audio = sd.rec(int(duration * sample_rate), samplerate=sample_rate, channels=2)
        sd.wait()

        bytes_io = io.BytesIO()
        sf.write(bytes_io, audio, sample_rate, format='wav')
        bytes_io.seek(0)

        audio_queue.put(bytes_io)

def recognize_speech():
    r = sr.Recognizer()
    while True:
        audio_file = audio_queue.get()
        
        with sr.AudioFile(audio_file) as source:
            audio_data = r.record(source)
            
            try:
                text = r.recognize_google(audio_data)
                text_queue.put(text)
                logger.info(text)
            except sr.UnknownValueError:
                # print("Speech recognition could not understand audio")
                pass
            except sr.RequestError as e:
                print(f"Could not request results from Google Speech Recognition service; {e}")


def start_workers():
    # Create separate threads for recording and speech recognition
    record_thread = threading.Thread(target=record_audio, daemon=True)
    recognize_thread = threading.Thread(target=recognize_speech, daemon=True)

    record_thread.start()
    recognize_thread.start()

    # To ensure all threads continue executing
    record_thread.join()
    recognize_thread.join()