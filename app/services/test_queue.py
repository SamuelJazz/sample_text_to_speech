import io
import json
import os
import queue
import threading
from time import sleep
import traceback
import random
import csv
import tempfile
import numpy as np
import whisper
import base64
import time
import argparse
import torch
import asyncio
import symbl


import requests
# import sounddevice as sd
import soundfile as sf
import speech_recognition as sr
from rev_ai import apiclient
# from google.cloud import speech
from google.cloud import speech_v1p1beta1 as speech
import openai
from rev_ai import apiclient as api
from rev_ai.models import MediaConfig
from rev_ai.streamingclient import RevAiStreamingClient
import assemblyai as aai
from deepgram import Deepgram

from app.helpers.logger import logger, show_error

# google_client = speech.SpeechClient.from_service_account_json('/home/ubuntu/ai-bot-agent-vdi/app/services/c-labs-ingredient-dataset-de3ad3216380.json')

text_queue = queue.Queue()
audio_queue = queue.Queue()
file_path = "/Users/thejazz/Code/Smarter.Codes/ai-bot-agent-vdi/app/services/test_audio_1.wav"

SAMPLE_RATE = 44100

# Wit_ai configuration
WIT_AI_KEY = "KJ7WIS5ARPQWB5XITM22WSLHHHQSX2FU"

# AssemblyAI configuration


def on_open(session_opened: aai.RealtimeSessionOpened):
  "This function is called when the connection has been established."

  print("Session ID:", session_opened.session_id)

def on_data(transcript: aai.RealtimeTranscript):
  "This function is called when a new transcript has been received."

  if not transcript.text:
    return

  if isinstance(transcript, aai.RealtimeFinalTranscript):
    print(transcript.text, end="\r\n")
  else:
    print(transcript.text, end="\r")

def on_error(error: aai.RealtimeError):
  "This function is called when the connection has been closed."

  print("An error occured:", error)

def on_close():
  "This function is called when the connection has been closed."

  print("Closing Session")


config = aai.TranscriptionConfig(format_text=True )
aai.settings.api_key = "c1fff569d04644cdac08dea8654a9925"
transcriber = aai.Transcriber(config=config)
transcriber = aai.RealtimeTranscriber(
  on_data=on_data,
  on_error=on_error,
  sample_rate=44_100,
  on_open=on_open, # optional
  on_close=on_close, # optional
)

# client_aai = aai.Client('c1fff569d04644cdac08dea8654a9925')

# IBM configuration
IBM_USERNAME = ""  # IBM Speech to Text usernames are strings of the form XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
IBM_PASSWORD = "Jazz2@1999jomon"  # IBM Speech to Text passwords are mixed-case alphanumeric strings

# AZURE configuration
AZURE_SPEECH_KEY=""

# Sembly configuration 
app_id = "597961634d34716a4d4b67614e74384f57367a6250386345753434324e364b31"
app_secret = "6155443346513663503163364b79756c6662577674425347544d72364c7778326a495f30444c33524a70424f6b774c4e6a5975563153345570446f734f555169"

# Deepgram configuration
DG_API_KEY = "3af8b22be13b3c72cc6f4aa0b75a3b1e3f513823"
dg_client = Deepgram("3af8b22be13b3c72cc6f4aa0b75a3b1e3f513823")
dg_url = "https://api.deepgram.com/v1/listen"
dg_headers = {
    "accept": "application/json",
    "content-type": "application/json",
    "Authorization": f"Bearer {DG_API_KEY}"
}

# OpenAI configuration
openai.api_key='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'

# Google cloud configuration
GOOGLE_CLOUD_SPEECH_CREDENTIALS = '/Users/thejazz/Code/Smarter.Codes/ai-bot-agent-vdi/app/services/c-labs-ingredient-dataset-de3ad3216380.json'

# Rev.ai configuration
token = "02fOekmsd52xoUlwmt2L6M1fC3hC2P-b9CtBn-czaVhpHYrf_v_DLGzYuDyzwb5h3f1wFLDv9c7H2rQAUgKTyV2PNRhfw"
# rev_client = apiclient.RevAiAPIClient(token)
config = MediaConfig("audio/x-wav", "interleaved", SAMPLE_RATE, "S16LE", 2)
config = MediaConfig("audio/x-wav")
rev_client = api.RevAiAPIClient(token)
streaming_client = RevAiStreamingClient(token,
                                        config)


def record_audio():
    temp_chunk_size=0  
    i=0
    while True:
        i+=1
        try:
            randum_chuck = random.randint(1,10)
            audio, sample_rate = sf.read(file_path)
            chunk_size = int(randum_chuck * sample_rate)

            chunk = audio[temp_chunk_size:temp_chunk_size+chunk_size]
            temp_chunk_size = temp_chunk_size + chunk_size
            bytes_io = io.BytesIO()
            sf.write(bytes_io, chunk, sample_rate, format='wav')
            bytes_io.seek(0)
            
            audio_queue.put((bytes_io, randum_chuck, i))

        except Exception as e:
            logger.exception(e)
            sleep(0.01)
        except:
            logger.error(traceback.format_exc())
            sleep(0.01)


def record_audio2():
    temp_chunk_size=0  
    i=0
    while True:
        i+=1
        try:
            randum_chuck = random.randint(1,10)
            audio, sample_rate = sf.read(file_path)
            chunk_size = int(randum_chuck * sample_rate)

            chunk = audio[temp_chunk_size:temp_chunk_size+chunk_size]
            temp_chunk_size = temp_chunk_size + chunk_size
            bytes_io = io.BytesIO()
            sf.write(bytes_io, chunk, sample_rate, format='wav')
            bytes_io.seek(0)
            buffered_reader = io.BufferedReader(bytes_io)
            audio_queue.put((buffered_reader, randum_chuck, i))

        except Exception as e:
            logger.exception(e)
            sleep(0.01)
        except:
            logger.error(traceback.format_exc())
            sleep(0.01)


def recognize_speech():
    r = sr.Recognizer()
    while True:
        print('recognize, -----------------')
        try:
            audio_file,randum_chuck,i = audio_queue.get()

            with sr.AudioFile(audio_file) as source:
                audio_data = r.record(source)


            

                try:
                    text = r.recognize_google(audio_data)
                    print("text",text)
                    # text_queue.put(text)
                    logger.info(text)

                    # Open the CSV file in append mode and write the data to it
                    with open('/Users/thejazz/Code/Smarter.Codes/ai-bot-agent-vdi/app/services/test.csv', 'a', newline='') as file:
                        writer = csv.writer(file)
                        writer.writerow([text, randum_chuck, i])

                except sr.UnknownValueError:
                    logger.debug("Could not understand audio")
                except sr.RequestError as e:
                    logger.error(e)
        except Exception as e:
            logger.exception(e)
            sleep(0.01)
        except:
            logger.error(traceback.format_exc())
            sleep(0.01)


# def sample_recognize(): 
#     while True:
#         print('recognize, -----------------')
#         try:
#             audio_file,randum_chuck,i = audio_queue.get()

#             # with open() as source:
#             #     audio_data = r.record(source)
#             #     print('audio_data',audio_data)

#             result = whisper.transcribe(audio_file)
#             print(result["text"])

#         except Exception as e:
#             logger.exception(e)
#             sleep(0.01)
#         except:
#             logger.error(traceback.format_exc())
#             sleep(0.01)



def recognize_speech_sembly_ai():
    r = sr.Recognizer()
    transcriber.connect()
    while True:
        print('recognize -----------------')
        try:
            audio_file, random_chunk, i = audio_queue.get()

            with sr.AudioFile(audio_file) as source:
                audio_data = r.record(source)

                # audio_data = audio_file

                # connection_object = symbl.Streaming.start_connection()

                # connection_object.subscribe({'message_response': lambda response: print('got this response from callback', response)})

                # connection_object.send_audio_from_mic()

                # Process audio file
                conversation_object = symbl.Audio.process_file(
                # credentials={app_id, app_secret}, #Optional, Don't add this parameter if you have symbl.conf file in your home directory
                file_path=audio_data.get_wav_data())

                # Printing transcription messages
                print(conversation_object.get_messages())

        except Exception as e:
            logger.exception(e)
            sleep(0.01)
        except:
            logger.error(traceback.format_exc())
            sleep(0.01)



def recognize_speech_assembly_ai():
    r = sr.Recognizer()
    transcriber.connect()
    while True:
        print('recognize -----------------')
        try:
            audio_file, random_chunk, i = audio_queue.get()

            with sr.AudioFile(audio_file) as source:
                audio_data = r.record(source)

                transcript = transcriber.stream(audio_data.get_wav_data())

                print(transcript)

                # transcript = transcriber.transcribe(audio_data.get_wav_data().decode('utf-8'))

                # transcript = transcriber.transcribe(base64.b64encode(audio_data.get_wav_data()).decode('utf-8'))

                # print(transcript.text.decode('utf-8'))




                # # Open the CSV file in append mode and write the data to it
                # with open('/Users/thejazz/Code/Smarter.Codes/ai-bot-agent-vdi/app/services/test.csv', 'a', newline='') as file:
                #     writer = csv.writer(file)
                #     writer.writerow([text, random_chunk, i, 'OpenAI Whisper'])

        except Exception as e:
            logger.exception(e)
            sleep(0.01)
        except:
            logger.error(traceback.format_exc())
            sleep(0.01)



def recognize_speech_deepgram():
    r = sr.Recognizer()
    loop = asyncio.new_event_loop()  # Create a new event loop
    asyncio.set_event_loop(loop)  # Assign it as the current loop
    loop = asyncio.get_event_loop()
    while True:
        print('recognize, -----------------')
        try:
            audio_file, randum_chuck, i = audio_queue.get()



            try:
                audio_data = {"buffer": audio_file, "mimetype": "audio/mpeg"}
                start_time = time.time()
                result =  dg_client.transcription.prerecorded(audio_data, {"model": "nova", "diarize": "true"})
                task = loop.create_task(result)
                result = loop.run_until_complete(task)
                words = result["results"]["channels"][0]["alternatives"][0]["words"]
                sentence = ' '.join(item['word'] for item in words)
                response_time = time.time() - start_time
                print("response time", response_time)
                print(sentence)

                # Open the CSV file in append mode and write the data to it
                with open('/Users/thejazz/Code/Smarter.Codes/ai-bot-agent-vdi/app/services/test_deepgram.csv', 'a', newline='') as file:
                        writer = csv.writer(file)
                        writer.writerow([i,randum_chuck, sentence,response_time ])

                    # response = requests.post(dg_url, data=payload, headers=dg_headers)
                    # print(response.text)

                    # response= deepgramLive.send(audio_data.content.readany())
                    # print("text", response)
                    # response = dg_client.transcription.prerecorded(audio_data, {'punctuate': True, 'interim_results': False, 'language': 'en-GB'})
                    # # result = asyncio.run(response)
                    # print("text", response)
                # response = dg_client.transcription.prerecorded(audio_file, {"model": "nova", "diarize": "true"} )
                #     # words = response["results"]["channels"][0]["alternatives"][0]["words"]
                #     # for word in words:
                #     #     print(word)
                # task = loop.create_task(response)  # Schedule the coroutine to run in the background
                # result = loop.run_until_complete(task)  # Execute the coroutine and get the result
                # print("text", result)
            except sr.UnknownValueError:
                logger.debug("Could not understand audio")
            except sr.RequestError as e:
                logger.error(e)
        except Exception as e:
            logger.exception(e)
            sleep(0.01)
        except:
            logger.error(traceback.format_exc())
            sleep(0.01)


def recognize_speech_wit_ai():
    r = sr.Recognizer()
    while True:
        print('recognize, -----------------')
        try:
            audio_file,randum_chuck,i = audio_queue.get()

            with sr.AudioFile(audio_file) as source:
                audio_data = r.record(source)

                try:
                    # Google Wit ai STT
                    text = r.recognize_wit(audio_data, key=WIT_AI_KEY)
                    print("text",text)

                except sr.UnknownValueError:
                    logger.debug("Could not understand audio")
                except sr.RequestError as e:
                    logger.error(e)
        except Exception as e:
            logger.exception(e)
            sleep(0.01)
        except:
            logger.error(traceback.format_exc())
            sleep(0.01)


def recognize_speech_IBM_ai():
    r = sr.Recognizer()
    while True:
        print('recognize, -----------------')
        try:
            audio_file,randum_chuck,i = audio_queue.get()

            with sr.AudioFile(audio_file) as source:
                audio_data = r.record(source)

                try:
                    # Google Cloud STT
                    text = r.recognize_ibm(audio_data, username=IBM_USERNAME, password=IBM_PASSWORD)
                    print("text",text)

                except sr.UnknownValueError:
                    logger.debug("Could not understand audio")
                except sr.RequestError as e:
                    logger.error(e)
        except Exception as e:
            logger.exception(e)
            sleep(0.01)
        except:
            logger.error(traceback.format_exc())
            sleep(0.01)


def recognize_speech_azur_ai():
    r = sr.Recognizer()
    while True:
        print('recognize, -----------------')
        try:
            audio_file,randum_chuck,i = audio_queue.get()

            with sr.AudioFile(audio_file) as source:
                audio_data = r.record(source)

                try:
                    # Google Cloud STT
                    text = r.recognize_azure(audio_data, key=AZURE_SPEECH_KEY)
                    print("text",text)

                except sr.UnknownValueError:
                    logger.debug("Could not understand audio")
                except sr.RequestError as e:
                    logger.error(e)
        except Exception as e:
            logger.exception(e)
            sleep(0.01)
        except:
            logger.error(traceback.format_exc())
            sleep(0.01)



def recognize_speech_whisper():
    model = whisper.load_model("base")
    r = sr.Recognizer()
    while True:
        print('recognize -----------------')
        try:
            audio_file, random_chunk, i = audio_queue.get()

            with sr.AudioFile(audio_file) as source:
                audio_data = r.record(source)

            audio = whisper.load_audio(audio_data.get_wav_data(), SAMPLE_RATE)

            mel = whisper.log_mel_spectrogram(audio).to(model.device)

            options = whisper.DecodingOptions()
            result = whisper.decode(model, mel, options)

            print("-----",result["text"])

            print("Transcript:", result["text"])

                
                # Convert BytesIO object to bytes
                # audio_bytes = audio_data.get_wav_data()

                # audio_data.get_raw_data(convert_rate=44100, convert_width=2)

                # Transcribe audio using OpenAI Whisper
                # response = openai.Audio.transcribe(
                #     file=audio_bytes,
                #     model="whisper-1",
                #     language="en",
                # )

                # # Retrieve the transcribed text from the response
                # text = response['transcriptions'][0]['text']
                # print("text:", text)

                # # Add the transcribed text to the text queue
                # text_queue.put(text)
                # logger.info(text)

                # # Open the CSV file in append mode and write the data to it
                # with open('/Users/thejazz/Code/Smarter.Codes/ai-bot-agent-vdi/app/services/test.csv', 'a', newline='') as file:
                #     writer = csv.writer(file)
                #     writer.writerow([text, random_chunk, i, 'OpenAI Whisper'])

        except Exception as e:
            logger.exception(e)
            sleep(0.01)
        except:
            logger.error(traceback.format_exc())
            sleep(0.01)


def recognize_speech_google_cloud():
    r = sr.Recognizer()
    loop = asyncio.new_event_loop()  # Create a new event loop
    asyncio.set_event_loop(loop)  # Assign it as the current loop
    loop = asyncio.get_event_loop()
    while True:
        print('recognize, -----------------')
        try:
            audio_file,randum_chuck,i = audio_queue.get()

            with sr.AudioFile(audio_file) as source:
                audio_data = r.record(source)

                try:
                    # Google Wit.ai STT
                    start_time = time.time()
                    start_time_test = time.time()
                    wit_text = r.recognize_wit(audio_data, key=WIT_AI_KEY)
                    end_time = time.time()
                    response_time_wit = end_time - start_time
                    print("Wit.ai response time:", response_time_wit)
                    print("Text:", wit_text)
                    end_time_test = time.time()
                    print("Test wit time: ",(end_time_test-start_time_test))

                    # Google Cloud STT
                    start_time = time.time()
                    start_time_test = time.time()
                    text = r.recognize_google_cloud(audio_data, credentials_json=GOOGLE_CLOUD_SPEECH_CREDENTIALS)
                    end_time = time.time()
                    response_time_google = end_time - start_time
                    print("Google Cloud STT response time:", response_time_google)
                    print("Text:", text)
                    text_queue.put(text)
                    logger.info(text)
                    end_time_test = time.time()
                    print("Test google time: ",(end_time_test-start_time_test))

                    # Rev.ai
                    start_time = time.time()
                    start_time_test = time.time()
                    MEDIA_GENERATOR = [audio_data.get_wav_data()]
                    response_generator = streaming_client.start(MEDIA_GENERATOR)   

                    transcript = ''

                    for response,job_id in response_generator:
                        response_data = json.loads(response)
                        if response_data.get("type") == "final":
                            transcript += ' '.join(element['value'] for element in response_data['elements'] if element.get('type') == 'text')
                            if response_data['elements'][-1]['type'] != 'punct':
                                transcript += ' '

                    end_time = time.time()
                    response_time_rev = end_time - start_time
                    print("Rev.ai response time:", response_time_rev)    
                    
                    print("transcript -----", transcript)
                    end_time_test = time.time()
                    print("Test Rev time: ",(end_time_test-start_time_test))

                    # Deepgram
                    audio_data = {"buffer": audio_file, "mimetype": "audio/mpeg"}
                    start_time = time.time()
                    result =  dg_client.transcription.prerecorded(audio_data, {"model": "nova", "diarize": "true"})
                    task = loop.create_task(result)
                    result = loop.run_until_complete(task)
                    words = result["results"]["channels"][0]["alternatives"][0]["words"]
                    deep_sentence = ' '.join(item['word'] for item in words)
                    end_time = time.time()
                    deep_time = end_time - start_time
                    print("response time", deep_time)
                    print(deep_sentence)

                    # Open the CSV file in append mode and write the data to it
                    with open('/Users/thejazz/Code/Smarter.Codes/ai-bot-agent-vdi/app/services/test.csv', 'a', newline='') as file:
                        writer = csv.writer(file)
                        writer.writerow([i,randum_chuck, text, response_time_google,transcript,response_time_rev ,  wit_text,response_time_wit,deep_sentence,deep_time])

                except sr.UnknownValueError:
                    logger.debug("Could not understand audio")
                except sr.RequestError as e:
                    logger.error(e)
        except Exception as e:
            logger.exception(e)
            sleep(0.01)
        except:
            logger.error(traceback.format_exc())
            sleep(0.01)

# def recognize_speech_google_cloud():
#     r = sr.Recognizer()
#     while True:
#         print('recognize, -----------------')
#         try:
#             audio_file, randum_chuck, i = audio_queue.get()
#             with sr.AudioFile(audio_file) as source:
#                 audio_data = r.record(source)

#                 client = speech.SpeechClient()

#                 # Configure the audio settings
#                 config = speech.RecognitionConfig(
#                     encoding=speech.RecognitionConfig.AudioEncoding.LINEAR16,
#                     sample_rate_hertz=16000,
#                     language_code="en-US",
#                 )
#                 audio = speech.RecognitionAudio(content=audio_data.get_wav_data())

#                 # Make the API request
#                 response = client.recognize(config=config, audio=audio)

#                 # Process the response
#                 for result in response.results:
#                     text = result.alternatives[0].transcript
#                     print("text", text)
#                     text_queue.put(text)
#                     logger.info(text)

#                     # Open the CSV file in append mode and write the data to it
#                     with open('/Users/thejazz/Code/Smarter.Codes/ai-bot-agent-vdi/app/services/test.csv',
#                               'a', newline='') as file:
#                         writer = csv.writer(file)
#                         writer.writerow([i,randum_chuck,text])

#         except Exception as e:
#             logger.exception(e)
#             sleep(0.01)
#         except:
#             logger.error(traceback.format_exc())
#             sleep(0.01)

def recognize_speech_rev_ai():
    r = sr.Recognizer()
    while True:
        print('recognize rev.ai, -----------------')
        try:
            audio_file, randum_chuck, i = audio_queue.get()
             
            with sr.AudioFile(audio_file) as source:
                audio_data = r.record(source)
            
                start_time=time.time()

                MEDIA_GENERATOR = [audio_data.get_wav_data()]

                response_generator = streaming_client.start(MEDIA_GENERATOR)

                # job = rev_client.submit_job_local_file(audio_file.getvalue())

                # transcript_text = rev_client.get_transcript_text('wyMdjzjlxTvsmhWo')
                # print("transcript-----",transcript_text)

                # response, job_id = next(response_generator)

                # for response, job_id in response_generator:
                #     print(job_id)
                
                # print(job_id,'----')
                # transcript_text = rev_client.get_transcript_text(job_id)
                # print("transcript-----",transcript_text)

                # job_ID = ''

                transcript = ''

                for response,job_id in response_generator:
                    response_data = json.loads(response)
                    # print(response_data.get('id'))
                    if response_data.get("type") == "final":
                        transcript += ' '.join(element['value'] for element in response_data['elements'] if element.get('type') == 'text')
                        if response_data['elements'][-1]['type'] != 'punct':
                            transcript += ' '
                    
                # start_time1=time.time()
                # transcript_text = rev_client.get_transcript_text(job_id)
                # end_time1=time.time()
                # print('Response Time',(end_time1-start_time1))
                end_time=time.time()
                print("transcript-----",transcript)
                print("Response Time",(end_time-start_time))

                # start_time = time.time()
                # MEDIA_GENERATOR = [audio_data.get_wav_data()]

                # response_generator = streaming_client.start(MEDIA_GENERATOR)

                # response, job_id = next(response_generator, (None, None))

                # transcript_text = ''
                # for response, job_id in response_generator:
                #     response_data = json.loads(response)
                #     if response_data.get("type") == "final":
                #         transcript_text += ' '.join(element['value'] for element in response_data['elements'] if element.get('type') == 'text')
                #         if response_data['elements'][-1]['type'] != 'punct':
                #             transcript_text += ' '

                # start_time1 = time.time()
                # transcript_text = rev_client.get_transcript_text(job_id)
                # end_time1 = time.time()
                # print('Response Time:', (end_time1 - start_time1))
                # end_time = time.time()
                # print("Transcript:", transcript_text)
                # print("Total Response Time:", (end_time - start_time))


        except Exception as e:
            logger.exception(e)
            sleep(0.01)
        except:
            logger.error(traceback.format_exc())
            sleep(0.01)



# def recognize_speech_rev_ai():
#     r = sr.Recognizer()
#     while True:
#         print('recognize rev.ai, -----------------')
#         try:
#             audio_file, randum_chuck, i = audio_queue.get()
#              # Save the audio file to disk
            
#             job = streaming_client.start(audio_file)
#             print('job-----',job)
#             streaming_client.end(job.id)

#             result = streaming_client.get_transcript_text(job.id)

#             if result.status == "transcribed":
#                 text = result.raw_response["monologues"][0]["elements"][0]["value"]
#                 text_queue.put(text)
#                 logger.info(text)

#                 # Open the CSV file in append mode and write the data to it
#                 with open('test.csv', 'a', newline='') as file:
#                     writer = csv.writer(file)
#                     writer.writerow([text, randum_chuck, i, 'Rev.ai'])

#         except Exception as e:
#             logger.exception(e)
#             sleep(0.01)
#         except:
#             logger.error(traceback.format_exc())
#             sleep(0.01)





# def recognize_speech_google():
#     while True:
#         try:
#             audio_file, chunk_length, index = audio_queue.get()

#             audio = speech.RecognitionAudio(content=audio_file.read())

#             config = speech.RecognitionConfig(
#                 encoding=speech.RecognitionConfig.AudioEncoding.LINEAR16,
#                 sample_rate_hertz=SAMPLE_RATE,
#                 language_code="en-US",
#             )

#             response = google_client.recognize(config=config, audio=audio)

#             for result in response.results:
#                 text = result.alternatives[0].transcript
#                 text_queue.put((text, chunk_length, index))
#                 logger.info(text)

#                 # Open the CSV file in append mode and write the data to it
#                 with open('test.csv', 'a', newline='') as file:
#                     writer = csv.writer(file)
#                     writer.writerow([text, chunk_length, index,'Google STT'])

#         except Exception as e:
#             logger.exception(e)
#             sleep(0.01)
#         except:
#             logger.error(traceback.format_exc())
#             sleep(0.01)

@show_error
def start_workers():
    record_thread = threading.Thread(target=record_audio, daemon=True)
    # record_tread1 = threading.Thread(target=record_audio2, daemon=True)
    # recognize_thread1 = threading.Thread(target=recognize_speech, daemon=True)
    # recognize_thread2 = threading.Thread(target=recognize_speech_google_cloud, daemon=True)
    # recognize_thread3 = threading.Thread(target=sample_recognize, daemon=True)
    # recognize_thread4 = threading.Thread(target=recognize_speech_rev_ai, daemon=True)
    # recognize_thread5 = threading.Thread(target=recognize_speech_whisper, daemon=True)
    # recognize_thread6 = threading.Thread(target=recognize_speech_assembly_ai, daemon=True)
    # recognize_thread7 = threading.Thread(target=recognize_speech_wit_ai, daemon=True)
    recognize_thread8 = threading.Thread(target=recognize_speech_deepgram, daemon=True)
    # recognize_thread9 = threading.Thread(target=recognize_speech_sembly_ai, daemon=True)
    # recognize_thread2 = threading.Thread(target=recognize_speech_google, daemon=True)

    record_thread.start()
    # record_tread1.start()
    # recognize_thread1.start()
    # recognize_thread2.start()
    # recognize_thread3.start()
    # recognize_thread4.start()
    # recognize_thread5.start()
    # recognize_thread6.start()
    # recognize_thread7.start()
    recognize_thread8.start()
    # recognize_thread9.start()

    record_thread.join()
    # record_tread1.join()

    # recognize_thread1.join()
    # recognize_thread2.join()
    # recognize_thread3.join()
    # recognize_thread4.join()
    # recognize_thread5.join()
    # recognize_thread6.join()
    # recognize_thread7.join()
    # perform_thread.join()
    recognize_thread8.join()
    # recognize_thread9.join()



