import threading
from os import environ

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.routers import session
from app.services.test_queue import start_workers

if environ.get('DEBUG'):
    import debugpy
    debugpy.listen(5678)
    debugpy.wait_for_client()

ROOTPATH = environ.get('ROOTPATH')
app = FastAPI(root_path=ROOTPATH)


@app.on_event("startup")
async def start_asr():
    threading.Thread(target=start_workers, daemon=True).start()


origins = [
    "http://localhost",
    "http://localhost:3000",
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.include_router(
    session.router,
    tags=["session"]
)
