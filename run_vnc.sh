until x11vnc -display $DISPLAY -quiet -shared -noxdamage -autoport -localhost -usepw -bg -xkb -ncache_cr -forever
do
    echo "####################################"
    echo "#                                  #"
    echo "# VNC failed to start. Retrying... #"
    echo "#                                  #"
    echo "####################################"

    sleep 3
done