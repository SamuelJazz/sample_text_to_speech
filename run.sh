#!/bin/bash

rm -f /tmp/.X1-lock
bash /app/run_display.sh &

mkdir -p ~/.vnc
x11vnc -storepasswd $VNCPASSWD ~/.vnc/passwd
bash /app/run_vnc.sh

websockify -D --web=/usr/share/novnc/ 6080 localhost:5900

bash /app/run_sound.sh

if [[ -z "${RELOAD}" ]]; then
    uvicorn app.main:app --app-dir /app --proxy-headers --host 0.0.0.0 --port 3000
else
    uvicorn app.main:app --app-dir /app --reload --reload-dir /app --proxy-headers --host 0.0.0.0 --port 3000
fi
